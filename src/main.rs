use anyhow::Result;
use async_tungstenite::{
    tungstenite::Message as WebSocketMessage,
    WebSocketStream,
};

use futures::{future, future::{join}};
use futures::stream::StreamExt;
use futures::sink::SinkExt;
use futures::channel::mpsc::{unbounded, UnboundedSender, UnboundedReceiver};

use rand::{Rng, seq::SliceRandom};
use serde::{Deserialize, Serialize};
use smol::{Async, Task};

use std::collections::HashMap;
use std::net::{SocketAddr, TcpListener, TcpStream};
use std::sync::{Arc, Mutex};

#[derive(Debug, Clone)]
enum GameState {
    NotRunning,
    InGame(GameHandle),
}

impl GameState {
    fn in_game(&self) -> bool {
        match self {
            GameState::NotRunning => false,
            GameState::InGame(_) => true,
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
enum ClientMessage {
    StartGame(GameStartData),
    Join(String),
    StartVote(Vec<usize>),
    Disconnected,
    Vote(bool),
    Assassinate(usize),
    RestartGame,
    // JS does not allow sending WS ping packets so a custom implementation is
    // needed.
    AvalonPing,
}

#[derive(Debug, Deserialize, Serialize)]
struct GameStartData {
    numplayers: usize,
    percival: bool,
    mordred: bool,
    oberon: bool,
    morgana: bool,
}

#[derive(Debug, Clone)]
struct GameHandle {
    write: UnboundedSender<(usize, WebSocketMessage)>,
    game: Arc<Mutex<Game>>,
}

impl GameHandle {
    async fn run_game(
        &mut self,
        mut read: UnboundedReceiver<(usize, WebSocketMessage)>,
        restart_game: UnboundedSender<()>,
    ) {
        while let Some((id, msg)) = read.next().await {
            let mut game = self.game.lock().unwrap();
            match msg {
                WebSocketMessage::Text(t) => {
                    if let Some(m) = serde_json::from_str::<ClientMessage>(&t).ok() {
                        if !game.on_message(id, m) {
                            restart_game.unbounded_send(()).unwrap();
                            for p in &mut game.players {
                                p.ws = None;
                            }
                            return;
                        }
                    }
                }
                WebSocketMessage::Ping(data) => {
                    let tx = &game.players[id].ws.as_ref().expect(
                        "Received message from player who is not connected"
                    );
                    if let Err(e) = tx.unbounded_send(WebSocketMessage::Pong(data)) {
                        eprintln!("Failed to send message to {}: {}", id, e);
                    }
                }
                _ => {
                }
            }
        }
    }
}

#[derive(Debug, Clone)]
struct Game {
    players: Vec<Player>,
    leader: usize,
    quests: Vec<Option<QuestResult>>,
    current_quest: usize,
    phase: GamePhase,
    team_votes: Vec<ClientVoteResult>
}

#[derive(Debug, Clone, Serialize)]
struct QuestResult {
    succeeded: bool,
    success_number: usize,
    fail_number: usize,
    team: Vec<usize>,
}

#[derive(Debug, Clone)]
struct Player {
    name: Option<String>,
    role: PlayerRole,
    ws: Option<UnboundedSender<WebSocketMessage>>,
}

impl Player {
    fn format_name(&self) -> String {
        self.name.clone().unwrap_or("".to_string())
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Serialize)]
enum PlayerRole {
    BasicServant,
    Merlin,
    Percival,
    BasicMinion,
    Assassin,
    Mordred,
    Oberon,
    Morgana,
}
use PlayerRole::*;

impl PlayerRole {
    fn is_evil(self) -> bool {
        match self {
            BasicServant => false,
            Merlin => false,
            Percival => false,
            BasicMinion => true,
            Assassin => true,
            Mordred => true,
            Oberon => true,
            Morgana => true,
        }
    }
}

impl ToString for PlayerRole {
    fn to_string(&self) -> String {
        match self {
            BasicServant => "A loyal servant of Arthur",
            Merlin => "Merlin",
            Percival => "Percival",
            BasicMinion => "A minion of Mordred",
            Assassin => "The Assassin",
            Mordred => "Mordred",
            Oberon => "Oberon",
            Morgana => "Morgana",
        }.to_string()
    }
}

#[derive(Debug, Clone)]
enum GamePhase {
    PickTeam { remaining_rejects: u8 },
    Vote(VoteState),
    Quest { results: HashMap<usize, Option<bool>> },
    Assassination,
    GameOver { winner: Side, reason: String }
}

impl GamePhase {
    fn is_vote(&self) -> bool {
        match self {
            GamePhase::Vote(_) => true,
            _ => false,
        }
    }
}

#[derive(Debug, Clone)]
struct VoteState {
    remaining_rejects: u8,
    team: Vec<usize>,
    votes: Vec<Option<bool>>,
}

fn send_to(players: &mut Vec<Player>, id: usize, msg: ServerMessage) {
    if let Some(ws) = players[id].ws.as_ref() {
        let encoded = serde_json::to_string(&msg)
            .unwrap_or_else(|_| panic!("Could not encode {:?}", msg));

        if let Err(e) = ws.unbounded_send(WebSocketMessage::Text(encoded)) {
            eprintln!("Failed to send message to {}: {}", id, e);
        }
    } else {
        eprintln!("Tried sending message to inactive player {}", id);
    }
}

fn broadcast(players: &mut Vec<Player>, message: ServerMessage) {
    let encoded = serde_json::to_string(&message)
        .unwrap_or_else(|_| panic!("Could not encode {:?}", message));
    broadcast_raw(players, encoded);
}

fn broadcast_raw(players: &mut Vec<Player>, message: String) {
    for (i, player) in players.iter_mut().enumerate() {
        if let Some(tx) = &player.ws {
            let msg = message.clone();

            if let Err(e) = tx.unbounded_send(WebSocketMessage::Text(msg)) {
                eprintln!(
                    "Failed to send message to {} ({}): {}", i, player.format_name(), e
                );
            }
        }
    }
}

enum VoteResult {
    Approve(Vec<usize>, Vec<bool>),
    Reject(Vec<usize>, u8, Vec<bool>),
    Continue,
    GameOver
}

impl Game {
    fn send_join_messages(&mut self, id: usize) {
        let role = self.players[id].role;
        let name = self.players[id].name.clone().expect("Unnamed player joined");

        for i in 0..self.players.len() {
            if i != id {
                let other_role = self.players[i].role;
                send_to(&mut self.players, i, ServerMessage::SetPlayer(
                    id,
                    client_player_data(name.clone(), other_role, role)
                ));
            }
        }

        let client_players = self.players.iter().map(|p| p.name.as_ref().map(|name| {
            client_player_data(
                name.clone(),
                role,
                p.role
            )
        })).collect();
        send_to(&mut self.players, id, ServerMessage::JoinedGame(JoinData {
            my_id: id,
            leader: self.leader,
            quests: self.quests.clone(),
            my_role: role,
            players: client_players,
            phase: game_phase_to_client(self.phase.clone()),
            team_votes: self.team_votes.clone(),
        }));
    }

    fn on_message(&mut self, id: usize, msg: ClientMessage) -> bool {
        match msg {
            ClientMessage::StartGame(_) => {
                eprintln!("Invalid client message {:?} from {}", msg, id);
            }
            ClientMessage::Join(_) => {
                eprintln!("Invalid client message {:?} from {}", msg, id);
            }
            ClientMessage::StartVote(team) => {
                if id != self.leader {
                    send_to(&mut self.players, id, ServerMessage::ShowAlert(
                        "Cannot select team when not leader".to_string()
                    ));
                    return true;
                }

                let correct_amount = self.current_quest_player_amount();
                if team.len() != correct_amount {
                    send_to(&mut self.players, id, ServerMessage::ShowAlert(
                        format!(
                            "Wrong amount of team members. You should pick {} people.",
                            correct_amount
                        )
                    ));
                    return true;
                }

                for p in &team {
                    if *p >= self.players.len() {
                        send_to(&mut self.players, id, ServerMessage::ShowAlert(
                            "Out of bounds player id".to_string()
                        ));
                        return true;
                    }
                }

                match self.phase {
                    GamePhase::PickTeam { remaining_rejects } => {
                        self.set_phase(GamePhase::Vote(VoteState {
                            remaining_rejects,
                            team: team.clone(),
                            votes: vec![None; self.players.len()],
                        }));
                    }
                    _ => {
                        send_to(&mut self.players, id, ServerMessage::ShowAlert(
                            "Not currently in the team picking phase".to_string()
                        ));
                    }
                }
            }
            ClientMessage::Vote(v) => {
                if self.phase.is_vote() {
                    self.manage_team_vote(id, v);
                } else {
                    self.set_quest_result(id, v);
                }
            }
            ClientMessage::Assassinate(target) => {
                match &mut self.phase {
                    GamePhase::Assassination => {
                        if self.players[id].role != Assassin {
                            send_to(&mut self.players, id, ServerMessage::ShowAlert(
                                "You are not the assassin".to_string()
                            ));
                        } else if target >= self.players.len() {
                            send_to(&mut self.players, id, ServerMessage::ShowAlert(
                                "Invalid assassination target".to_string()
                            ));
                        } else {
                            let evil_won = self.players[target].role == Merlin;
                            self.set_phase(GamePhase::GameOver {
                                winner: if evil_won {
                                    Side::Evil
                                } else {
                                    Side::Good
                                },
                                reason: self.players[target].role.to_string() +
                                    " was assassinated."
                            });
                        }
                    }
                    _ => {
                        send_to(&mut self.players, id, ServerMessage::ShowAlert(
                            "Not currently in the assassination phase".to_string()
                        ));
                    }
                }
            }
            ClientMessage::RestartGame => {
                broadcast_raw(&mut self.players, "Restart".to_string());
                return false;
            }
            ClientMessage::AvalonPing => {
                send_to(&mut self.players, id, ServerMessage::AvalonPong);
            }
            ClientMessage::Disconnected => {
                println!("{} disconnected", id);
                self.players[id].ws = None;
            }
        }

        true
    }

    fn set_phase(&mut self, phase: GamePhase) {
        self.phase = phase;
        broadcast(
            &mut self.players,
            ServerMessage::SetPhase(game_phase_to_client(self.phase.clone()))
        );
    }

    fn manage_team_vote(&mut self, id: usize, approve: bool) {
        match self.vote_for_team(id, approve) {
            Ok(VoteResult::Approve(team, votes)) => {
                self.set_phase(GamePhase::Quest {
                    results: team.iter().map(|i| (*i, None)).collect(),
                });
                self.team_votes.push(ClientVoteResult {
                    approved: true,
                    team: team.clone(),
                    votes: votes.clone(),
                });
                self.broadcast_vote_results(true, team, votes);
            }
            Ok(VoteResult::Reject(team, remaining_rejects, votes)) => {
                self.set_phase(GamePhase::PickTeam { remaining_rejects });
                self.team_votes.push(ClientVoteResult {
                    approved: false,
                    team: team.clone(),
                    votes: votes.clone(),
                });

                self.leader = (self.leader + 1) % self.players.len();
                broadcast(
                    &mut self.players,
                    ServerMessage::SetLeader(self.leader),
                );
                self.broadcast_vote_results(false, team, votes);
            }
            Ok(VoteResult::GameOver) =>
                self.set_phase(GamePhase::GameOver {
                    winner: Side::Evil,
                    reason: "The servants of Arthur could not agree on a team.".to_string()
                }),
            Ok(VoteResult::Continue) => {}
            Err(msg) => {
                send_to(&mut self.players, id, ServerMessage::ShowAlert(
                    msg.to_string()
                ));
            }
        }
    }

    fn broadcast_vote_results(&mut self, approved: bool, team: Vec<usize>, votes: Vec<bool>) {
        broadcast(
            &mut self.players,
            ServerMessage::SetVoteResult(ClientVoteResult{approved, team, votes})
        );
    }

    fn vote_for_team(&mut self, id: usize, approve: bool) -> Result<VoteResult, &'static str> {
        match &mut self.phase {
            GamePhase::Vote(votestate) => {
                if let Some(_) = votestate.votes[id] {
                    return Err("You have already voted")
                }

                votestate.votes[id] = Some(approve);

                if votestate.votes.iter().all(|v| v.is_some()) {
                    let vote_results: Vec<bool> =
                        votestate.votes.iter().filter_map(|v| *v).collect();

                    let approves = vote_results.iter().filter(|v| **v).count();

                    if approves > votestate.votes.len() - approves {
                        Ok(VoteResult::Approve(votestate.team.clone(), vote_results))
                    } else {
                        if votestate.remaining_rejects > 1 {
                            // Team was not approved,
                            // pick a new team with the next leader
                            Ok(VoteResult::Reject(votestate.team.clone(), votestate.remaining_rejects - 1, vote_results))
                        } else {
                            // Team was not approved and the good team is
                            // out of tries
                            Ok(VoteResult::GameOver)
                        }
                    }
                } else {
                    Ok(VoteResult::Continue)
                }
            }
            _ => {
                Err("Not currently in the voting phase")
            }
        }
    }

    fn set_quest_result(&mut self, id: usize, success: bool) {
        match &mut self.phase {
            GamePhase::Quest { results } => {
                match results.get(&id) {
                    Some(Some(_)) => {
                        send_to(&mut self.players, id, ServerMessage::ShowAlert(
                            "You have already sent your result".to_string()
                        ));
                    }
                    None => {
                        send_to(&mut self.players, id, ServerMessage::ShowAlert(
                            "You are not on the quest team".to_string()
                        ));
                    }
                    Some(None) => {
                        results.insert(id, Some(success));

                        if results.values().all(|v| v.is_some()) {
                            let successes =
                                results.values().filter(|v| **v == Some(true)).count();

                            let team_size = results.values().count();
                            let fails = team_size - successes;

                            let needs_2_for_fail =
                                self.players.len() >= 7 && self.current_quest == 3;

                            let quest_result = QuestResult {
                                succeeded: if needs_2_for_fail {
                                    fails < 2
                                } else {
                                    fails < 1
                                },
                                success_number: successes,
                                fail_number: fails,
                                team: results.keys().cloned().collect(),
                            };
                            self.quests[self.current_quest] = Some(quest_result.clone());
                            broadcast(
                                &mut self.players,
                                ServerMessage::SetQuestResult(
                                    self.current_quest,
                                    quest_result,
                                ),
                            );

                            fn quest_succeeded(q: &&Option<QuestResult>) -> bool {
                                match *q {
                                    Some(QuestResult { succeeded, .. }) => *succeeded,
                                    _ => false,
                                }
                            };
                            fn quest_failed(q: &&Option<QuestResult>) -> bool {
                                match *q {
                                    Some(QuestResult { succeeded, .. }) => !succeeded,
                                    _ => false,
                                }
                            };

                            // Check if the game is over
                            let good_won =
                                self.quests.iter().filter(quest_succeeded).count() >= 3;
                            let evil_won =
                                self.quests.iter().filter(quest_failed).count() >= 3;

                            if good_won {
                                self.set_phase(GamePhase::Assassination);
                            } else if evil_won {
                                self.set_phase(GamePhase::GameOver {
                                    winner: Side::Evil,
                                    reason: "Three quests failed.".to_string()
                                });
                            } else {
                                self.current_quest += 1;

                                self.set_phase(GamePhase::PickTeam {
                                    remaining_rejects: 5
                                });

                                self.leader = (self.leader + 1) % self.players.len();
                                broadcast(
                                    &mut self.players,
                                    ServerMessage::SetLeader(self.leader),
                                );
                            }
                        }
                    }
                }
            }
            _ => {
                send_to(&mut self.players, id, ServerMessage::ShowAlert(
                    "Not currently in the quest phase".to_string()
                ));
            }
        }
    }

    fn quests_player_amounts(&self) -> [usize; 5] {
        match self.players.len() {
            5 => {
                [2, 3, 2, 3, 3]
            }
            6 => {
                [2, 3, 4, 3, 4]
            }
            7 => {
                [2, 3, 3, 4, 4]
            }
            8 => {
                [3, 4, 4, 5, 5]
            }
            9 => {
                [3, 4, 4, 5, 5]
            }
            10 => {
                [3, 4, 4, 5, 5]
            }
            n => {
                panic!("Invalid number of players {}", n);
            }
        }
    }

    fn current_quest_player_amount(&self) -> usize {
        self.quests_player_amounts()[self.current_quest]
    }
}

fn insert_player(
    players: &mut Vec<Player>,
    name: String,
    tx: UnboundedSender<WebSocketMessage>
) -> Option<usize> {
    // Don't add player if already in game
    for (i, player) in players.iter_mut().enumerate() {
        if (&player.name).as_ref() == Some(&name) {
            // Reuse ws if disconnected
            if player.ws.is_none() {
                player.ws = Some(tx);
                return Some(i);
            } else {
                return None;
            }
        }
    }

    // Player not already in game
    for (i, player) in players.iter_mut().enumerate() {
        if player.name.is_none() {
            player.name = Some(name);
            player.ws = Some(tx);
            return Some(i);
        }
    }

    None
}

#[derive(Clone, Copy, Debug, Serialize)]
enum Side { Good, Evil }

#[derive(Clone, Debug, Serialize)]
enum ServerMessage {
    ShowStartForm,
    GameStarted,
    ShowAlert(String),
    JoinedGame(JoinData),
    JoinFailed(String),
    SetPlayer(usize, ClientPlayer),
    SetLeader(usize),
    SetPhase(ClientPhase),
    SetQuestResult(usize, QuestResult),
    SetVoteResult(ClientVoteResult),
    AvalonPong,
}

#[derive(Clone, Debug, Serialize)]
struct ClientVoteResult {
    approved: bool,
    team: Vec<usize>,
    votes: Vec<bool>
}

#[derive(Clone, Debug, Serialize)]
struct JoinData {
    my_id: usize,
    my_role: PlayerRole,
    players: Vec<Option<ClientPlayer>>,
    leader: usize,
    quests: Vec<Option<QuestResult>>,
    phase: ClientPhase,
    team_votes: Vec<ClientVoteResult>,
}

#[derive(Clone, Debug, Serialize)]
struct ClientPlayer {
    name: String,
    evil: bool,
    merlin: bool,
}

#[derive(Clone, Debug, Serialize)]
enum ClientPhase {
    PickTeam(u8),
    Vote { remaining_rejects: u8, team: Vec<usize> },
    Quest(Vec<usize>),
    Assassination,
    GameOver{ winner: Side, reason: String }
}

fn game_phase_to_client(phase: GamePhase) -> ClientPhase {
    match phase {
        GamePhase::PickTeam { remaining_rejects } => {
            ClientPhase::PickTeam(remaining_rejects)
        }
        GamePhase::Vote(VoteState { team, remaining_rejects, .. }) => {
            ClientPhase::Vote { team, remaining_rejects }
        }
        GamePhase::Quest { results } => {
            ClientPhase::Quest(results.keys().cloned().collect())
        }
        GamePhase::Assassination => {
            ClientPhase::Assassination
        }
        GamePhase::GameOver { winner, reason } => {
            ClientPhase::GameOver { winner, reason }
        }
    }
}

fn client_player_data(other_name: String, me: PlayerRole, other: PlayerRole) -> ClientPlayer {
    let mut evil = false;
    let mut merlin = false;

    use PlayerRole::*;

    if me == Merlin {
        if other.is_evil() && other != Mordred {
            evil = true;
        }
    }

    if me == Percival {
        if other == Merlin || other == Morgana {
            merlin = true;
        }
    }

    if me.is_evil() && me != Oberon {
        if other.is_evil() && other != Oberon {
            evil = true;
        }
    }

    if me == other && me == Oberon {
        evil = true
    }

    ClientPlayer { name: other_name, evil, merlin }
}

fn new_game(game_start_data: GameStartData) -> Result<Game, ServerMessage> {
    let number_of_evil = match game_start_data.numplayers {
        5 => 2,
        6 => 2,
        7 => 3,
        8 => 3,
        9 => 3,
        10 => 4,
        _ => {
            return Err(ServerMessage::ShowAlert(
                "Invalid number of players".to_string()
            ));
        }
    };

    let chosen_evil = [
        game_start_data.mordred,
        game_start_data.oberon,
        game_start_data.morgana,
    ];
    let chosen_evil_count =
        chosen_evil.iter().filter(|c| **c).count();

    // Check that too many evil characters where not enabled.
    // Subtract 1 for assasin
    if chosen_evil_count > number_of_evil - 1 {
        return Err(ServerMessage::ShowAlert(
            "Too many optional evil characters".to_string()
        ));
    }

    let mut roles = vec![
        BasicServant; game_start_data.numplayers
    ];
    for i in 0..number_of_evil {
        roles[i] = BasicMinion;
    }

    // Add special evil characters
    roles[0] = Assassin;
    {
        let mut i = 1;
        if game_start_data.mordred {
            roles[i] = Mordred;
            i += 1;
        }
        if game_start_data.oberon {
            roles[i] = Oberon;
            i += 1;
        }
        if game_start_data.morgana {
            roles[i] = Morgana;
        }
    }

    // Add special good characters
    roles[number_of_evil] = Merlin;
    if game_start_data.percival {
        roles[number_of_evil+1] = Percival;
    }

    roles.shuffle(&mut rand::thread_rng());
    Ok(Game {
        players: roles.iter().map(|r| Player {
            role: *r,
            name: None,
            ws: None,
        }).collect(),
        leader: rand::thread_rng().gen_range(0, game_start_data.numplayers),
        quests: vec![None; 5],
        current_quest: 0,
        phase: GamePhase::PickTeam { remaining_rejects: 5 },
        team_votes: Vec::new(),
    })
}

fn start_game(game_state: &mut GameState, game_handle: GameHandle) -> bool {
    match &*game_state {
        GameState::NotRunning => {
            *game_state = GameState::InGame(game_handle);
            true
        }
        GameState::InGame(_) => {
            false
        }
    }
}

async fn handle_start_message(
    game_state: Arc<Mutex<GameState>>,
    ws_stream: &mut WebSocketStream<Async<TcpStream>>,
    game_start_data: GameStartData,
    restart_game: UnboundedSender<()>,
) -> Result<()> {
    let (write, read) = unbounded();
    let game = match new_game(game_start_data) {
        Ok(game) => {
            Arc::new(Mutex::new(game))
        }
        Err(msg) => {
            let encoded = serde_json::to_string(&msg).unwrap();
            ws_stream.send(WebSocketMessage::Text(encoded)).await?;
            return Ok(());
        }
    };
    let handle = GameHandle { write, game };

    let could_start = {
        let mut game_state = game_state.lock().unwrap();
        start_game(&mut game_state, handle.clone())
    };

    if !could_start {
        let msg = ServerMessage::ShowAlert(
            "A game is already running".to_string()
        );
        let encoded = serde_json::to_string(&msg).unwrap();
        ws_stream.send(WebSocketMessage::Text(encoded)).await?;
        return Ok(());
    }

    let msg = ServerMessage::GameStarted;
    let encoded = serde_json::to_string(&msg).unwrap();
    ws_stream.send(WebSocketMessage::Text(encoded)).await?;

    let mut h = handle.clone();
    Task::spawn(async move {
        h.run_game(read, restart_game).await;
    }).detach();

    Ok(())
}

async fn handle_join_message(
    game_state: Arc<Mutex<GameState>>,
    ws_stream: &mut WebSocketStream<Async<TcpStream>>,
    name: String,
    addr: SocketAddr,
) -> Result<()> {
    let maybe_handle = match &*game_state.lock().unwrap() {
        GameState::NotRunning => {
            None
        }
        GameState::InGame(handle) => {
            Some(handle.clone())
        }
    };

    if maybe_handle.is_none() {
        let msg = ServerMessage::JoinFailed(
            "The game is not running".to_string()
        );
        let encoded = serde_json::to_string(&msg).unwrap();
        ws_stream.send(WebSocketMessage::Text(encoded)).await?;
        return Ok(());
    }
    let handle = maybe_handle.unwrap();

    let (ws_tx, ws_rx) = unbounded();

    let maybe_id = {
        let game = &mut handle.game.lock().unwrap();
        insert_player(&mut game.players, name.clone(), ws_tx.clone())
    };

    if maybe_id.is_none() {
        let msg = ServerMessage::JoinFailed(
            "The game is already full".to_string()
        );
        let encoded = serde_json::to_string(&msg).unwrap();
        ws_stream.send(WebSocketMessage::Text(encoded)).await?;
        return Ok(());
    }
    let my_id = maybe_id.unwrap();

    let (outgoing, incoming) = ws_stream.split();
    let write = handle.write.clone();

    let ra = ws_rx
        .map(Ok).forward(outgoing);

    let rb = incoming
        .map(|m| match m {
            Ok(msg) => {
                Some(msg)
            }
            _ => {
                None
            }
        })
        .take_while(|m| future::ready(m.is_some()))
        .map(|m| m.unwrap())
        .chain(futures::stream::once(async {
            let msg = ClientMessage::Disconnected;
            let encoded = serde_json::to_string(&msg).unwrap();
            WebSocketMessage::Text(encoded)
        }))
        .map(move |m| Ok((my_id, m)))
        .forward(write);

    {
        let game = &mut handle.game.lock().unwrap();
        game.send_join_messages(my_id);
    };

    let (ra, rb) = join(ra, rb).await;

    if let Err(e) = ra {
        eprintln!(
            "[{}] Got error {} from player {}'s rx queue", addr, e, name
        );
    }
    if let Err(e) = rb {
        eprintln!(
            "[{}] Got error {} from player {}'s tx queue", addr, e, name
        );
    }

    Ok(())
}

async fn handle_connection(
    game_state: Arc<Mutex<GameState>>,
    raw_stream: Async<TcpStream>,
    addr: SocketAddr,
    restart_game: UnboundedSender<()>,
) -> Result<()> {
    let mut ws_stream = async_tungstenite::accept_async(raw_stream).await?;
    println!("[{}] WebSocket connection established", addr);

    // Tell client to show name form if already started
    {
        let in_game = game_state.lock().unwrap().in_game();
        let msg = if in_game {
            ServerMessage::GameStarted
        } else {
            ServerMessage::ShowStartForm
        };
        let encoded = serde_json::to_string(&msg).unwrap();
        ws_stream.send(WebSocketMessage::Text(encoded)).await?;
    }

    while let Some(Ok(msg)) = ws_stream.next().await {
        match msg {
            WebSocketMessage::Ping(data) => {
                ws_stream.send(WebSocketMessage::Pong(data)).await?;
            }
            WebSocketMessage::Text(data) => {
                let msg = serde_json::from_str::<ClientMessage>(&data)?;

                match msg {
                    ClientMessage::StartGame(game_start_data) => {
                        handle_start_message(
                            game_state.clone(),
                            &mut ws_stream,
                            game_start_data,
                            restart_game.clone(),
                        ).await?;
                    }
                    ClientMessage::Join(name) => {
                        handle_join_message(
                            game_state.clone(),
                            &mut ws_stream,
                            name,
                            addr,
                        ).await?;
                    }
                    msg => {
                        eprintln!("[{}] Got unexpected message {:?}", addr, msg);
                        break;
                    }
                }
            }
            WebSocketMessage::Close(_) => {
                break;
            }
            _ => {
            }
        }
    }

    println!("[{}] Dropping connection", addr);

    Ok(())
}

fn main() {
    let state = Arc::new(Mutex::new(GameState::NotRunning));
    let addr = std::env::args()
        .nth(1)
        .unwrap_or_else(|| "127.0.0.1:30000".to_string());

    for _ in 0 .. num_cpus::get().max(1) {
        std::thread::spawn(|| smol::run(future::pending::<()>()));
    }

    let restart_game = {
        let (tx, mut rx) = unbounded();
        let state = state.clone();
        Task::spawn(async move {
            while let Some(()) = rx.next().await {
                *state.lock().unwrap() = GameState::NotRunning;
            }
        }).detach();
        tx
    };

    smol::block_on(async {
        let listener = Async::<TcpListener>::bind(&addr)
            .expect("Could not create listener");

        println!("Listening on {}", &addr);

        while let Ok((stream, addr)) = listener.accept().await {
            let restart_game = restart_game.clone();
            let state = state.clone();

            println!("new connection {}", addr);

            Task::spawn(async move {
                if let Err(e) = handle_connection(state, stream, addr, restart_game).await {
                    eprintln!("Failed to handle connection from {}: {}", addr, e);
                }
            }).detach();
        }
    });

    println!("Shutting down.");

}
