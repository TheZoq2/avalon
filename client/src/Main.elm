port module Main exposing (..)

import Array exposing (Array)
import Browser
import Json.Decode as D
import Json.Encode as E
import Html.Styled

import Model exposing (..)
import View exposing (..)


updateArray : Int -> (a -> a) -> Array a -> Array a
updateArray i f xs =
    case Array.get i xs of
        Just x ->
            Array.set i (f x) xs

        Nothing ->
            xs


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , view = view >> Html.Styled.toUnstyled
        , update = update
        , subscriptions = subscriptions
        }


port sendMessage : String -> Cmd msg
port messageReceiver : (String -> msg) -> Sub msg

port storeName : String -> Cmd msg
port storedName : (String -> msg) -> Sub msg

port showAlert : String -> Cmd msg

port restartGame : () -> Cmd msg


init : () -> ( Model, Cmd Msg )
init flags =
    ( { storedName = Nothing
      , state = Loading
      }
    , Cmd.none
    )


initStartGameForm : StartGameForm
initStartGameForm =
    { numplayers = 5
    , characters =
        List.map (\(name, descr) ->
                      { fieldname = name
                      , description = descr
                      , checked = False
                      }
                 )
            [ ("percival", "Include Percival (knows Merlin)")
            , ("mordred", "Include Mordred (unknown to Merlin)")
            , ("oberon", "Include Oberon (unknown to evil)")
            , ("morgana", "Include Morgana (appears as Merlin)")
            ]
    }


initPlayer : String -> Bool -> Bool -> Player
initPlayer name evil merlin =
    Player name evil merlin False


serverMessageDecoder : D.Decoder ServerMessage
serverMessageDecoder =
    D.oneOf
        [ D.map GotServerTopMessage serverTopMessageDecoder
        , D.map GotServerGameMessage serverGameMessageDecoder
        ]


serverTopMessageDecoder : D.Decoder ServerTopMessage
serverTopMessageDecoder =
    D.oneOf
        [ exactStringDecoder "ShowStartForm" ShowStartForm
        , exactStringDecoder "GameStarted" GameStarted
        , D.field "ShowAlert" (D.map ShowAlert D.string)
        , D.field "JoinedGame"
            (D.map JoinedGame
                 (D.map7 initGameData
                      (D.field "my_id" D.int)
                      (D.field "my_role" roleDecoder)
                      (D.field "players" (D.array (D.nullable playerDecoder)))
                      (D.field "leader" D.int)
                      (D.field "quests" (D.array (D.nullable questResultDecoder)))
                      (D.field "phase" phaseDecoder)
                      (D.field "team_votes" teamVotesDecoder)))
        ]


serverGameMessageDecoder : D.Decoder ServerGameMessage
serverGameMessageDecoder =
    D.oneOf
        [ D.field "SetPlayer"
            (D.map2 SetPlayer
                (D.index 0 D.int)
                (D.index 1 playerDecoder))
        , D.field "SetQuestResult"
            (D.map2 SetQuestResult
                 (D.index 0 D.int)
                 (D.index 1 questResultDecoder))
        , D.field "SetPhase" (D.map SetPhase phaseDecoder)
        , D.field "SetLeader" (D.map SetLeader D.int)
        , D.field "SetVoteResult"
            (D.map SetVoteResult
                 (D.map3 VoteResult
                      (D.field "approved" D.bool)
                      (D.field "team" (D.list D.int))
                      (D.field "votes" (D.list D.bool))))
        ]


playerDecoder : D.Decoder Player
playerDecoder =
    D.map3 initPlayer
        (D.field "name" D.string)
        (D.field "evil" D.bool)
        (D.field "merlin" D.bool)


questResultDecoder : D.Decoder QuestResult
questResultDecoder =
    D.map4 QuestResult
        (D.field "succeeded" D.bool)
        (D.field "success_number" D.int)
        (D.field "fail_number" D.int)
        (D.field "team" (D.array D.int))

phaseDecoder : D.Decoder GamePhase
phaseDecoder =
    D.oneOf
        [ D.field "PickTeam"
              (D.map PickTeam D.int)
        , D.field "Vote"
            (D.map2 Vote
                 (D.field "remaining_rejects" D.int)
                 (D.field "team"
                      (D.list D.int)))
        , D.field "Quest"
            (D.map Quest
                 (D.list D.int))
        , exactStringDecoder "Assassination" Assassination
        , D.field "GameOver"
            (D.map2 GameOver
                 (D.field "winner"
                      (D.oneOf
                           [ exactStringDecoder "Good" Good
                           , exactStringDecoder "Evil" Evil
                           ]))
                 (D.field "reason" D.string))
        ]


teamVotesDecoder : D.Decoder (List VoteResult)
teamVotesDecoder =
    D.list
        (D.map3 VoteResult
             (D.field "approved" D.bool)
             (D.field "team" (D.list D.int))
             (D.field "votes" (D.list D.bool)))


roleDecoder : D.Decoder PlayerRole
roleDecoder =
    D.oneOf
        [ exactStringDecoder "BasicServant" BasicServant
        , exactStringDecoder "Merlin" Merlin
        , exactStringDecoder "Percival" Percival
        , exactStringDecoder "BasicMinion" BasicMinion
        , exactStringDecoder "Assassin" Assassin
        , exactStringDecoder "Mordred" Mordred
        , exactStringDecoder "Oberon" Oberon
        , exactStringDecoder "Morgana" Morgana
        ]


exactStringDecoder : String -> a -> D.Decoder a
exactStringDecoder expected value =
    let
        inner str =
            if str == expected then
                D.succeed value
            else
                D.fail (str ++ " is not " ++ expected)
    in
        D.string
            |> D.andThen inner
            


encodeStartGameData : StartGameForm -> String
encodeStartGameData startform =
    let
        encodedFields =
            List.map
                (\c ->
                     ( c.fieldname
                     , E.bool c.checked
                     )
                )
                startform.characters
    in
        E.encode 0
            ( E.object
                  [ ( "StartGame"
                    , E.object
                        ( [ ( "numplayers", E.int startform.numplayers )
                          ] ++ encodedFields)
                    )
                  ]
            )


encodeJoin : String -> String
encodeJoin name =
    E.encode 0 (E.object [( "Join", E.string name )])


encodeStartVote : List Int -> String
encodeStartVote team =
    E.encode 0 (E.object [( "StartVote", E.list E.int team )])


encodeAssassinate : Int -> String
encodeAssassinate target =
    E.encode 0 (E.object [( "Assassinate", E.int target )])


encodeVote : Bool -> String
encodeVote target =
    E.encode 0 (E.object [( "Vote", E.bool target )])


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case (msg, model.state) of
        (Recv ShowStartForm, _) ->
            ( { model | state = NotStarted initStartGameForm }
            , Cmd.none
            )

        (Recv GameStarted, _) ->
            case model.storedName of
                Just name ->
                    ( { model | state = Join name }
                    , sendMessage (encodeJoin name)
                    )

                Nothing ->
                    ( { model | state = Join "" }
                    , Cmd.none
                    )

        (Recv (JoinedGame gamedata), _) ->
            ( { model | state = InGame gamedata }
            , Cmd.none
            )

        (Recv (ShowAlert errorMsg), _) ->
            ( model
            , showAlert errorMsg
            )

        (JsonError e, _) ->
            ( model
            , showAlert (D.errorToString e)
            )

        (StoredName name, _) ->
            ( { model | storedName = Just name }
            , Cmd.none
            )

        (_, Loading) ->
            (model, Cmd.none)

        (GotStartGameMsg m, NotStarted startform) ->
            let
                (form, cmd) = updateStartForm m startform
            in
                ( { model | state = NotStarted form }
                , Cmd.map GotStartGameMsg cmd
                )

        (GotJoinMsg m, Join name) ->
            let
                (newName, cmd) = updateJoinForm m name
            in
                ( { model | state = Join newName }
                , Cmd.map GotJoinMsg cmd
                )
                
        (GotGameMsg m, InGame game) ->
            let
                (newGame, cmd) = updateGame m game
            in
                ( { model | state = InGame newGame }
                , Cmd.map GotGameMsg cmd
                )

        (_, _) ->
            -- Message arrived for the wrong page
            ( model, Cmd.none )


updateStartForm : StartGameMsg -> StartGameForm -> ( StartGameForm, Cmd StartGameMsg )
updateStartForm msg startform =
    case msg of
        SetNumPlayers num ->
            ( { startform | numplayers =
                    Maybe.withDefault 5 (String.toInt num)
              }
            , Cmd.none
            )

        ToggleCheck name ->
            ( { startform | characters =
                    List.map
                        (\c ->
                             if c.fieldname == name then
                                 { c | checked = not c.checked }
                             else
                                 c)
                        startform.characters
              }
            , Cmd.none
            )

        SubmitStartGame ->
            ( startform
            , sendMessage (encodeStartGameData startform)
            )


updateJoinForm : JoinMsg -> String -> ( String, Cmd JoinMsg )
updateJoinForm msg name =
    case msg of
        EditName newName ->
            ( newName , Cmd.none )

        SubmitJoin ->
            ( name
            , Cmd.batch
                [ sendMessage (encodeJoin name)
                , storeName name
                ]
            )


updateGame : GameMsg -> GameData -> ( GameData, Cmd GameMsg )
updateGame msg game =
    case msg of
        GameRecv (SetPlayer id playerData) ->
            ( { game | players =
                    Array.set id (Just playerData) game.players
              }
            , Cmd.none
            )

        GameRecv (SetLeader id) ->
            ( { game | leader = id }, Cmd.none )

        GameRecv ( SetPhase phase ) ->
            ( let
                  localGameState = game.localGameState
              in
                  { game
                      | phase = phase
                      , localGameState = { localGameState | alreadyVoted = False }
                  }
            , Cmd.none
            )

        GameRecv (SetQuestResult index res) ->
            ( { game | questResults = Array.set index (Just res) game.questResults }
            , Cmd.none
            )

        GameRecv (SetVoteResult vr) ->
            ( { game | teamVotes = game.teamVotes ++ [vr] }
            , Cmd.none
            )

        TogglePlayerCheck id ->
            ( { game | players =
                    updateArray id (Maybe.map togglePlayerCheck) game.players
              }
            , Cmd.none
            )

        StartVote ->
            ( game
            , sendMessage
                  (Array.indexedMap Tuple.pair game.players
                       |> Array.toList
                       |> List.filterMap
                          (\(i, mp) ->
                               Maybe.andThen
                                   (\p -> if p.checked then
                                              Just i
                                          else
                                              Nothing)
                                   mp)
                       |> encodeStartVote)
            )

        SendVote v ->
            ( let
                  localGameState = game.localGameState
              in
                  { game | localGameState =
                        { localGameState | alreadyVoted = True }
                  }
            , sendMessage (encodeVote v)
            )

        SetAssassinationTarget target ->
            ( case String.toInt target of
                  Just t ->
                      let
                          localGameState = game.localGameState
                      in
                          { game | localGameState =
                                { localGameState | assassinationTarget = t}
                          }

                  Nothing ->
                      game
            , Cmd.none
            )

        Assassinate ->
            ( game
            , sendMessage (encodeAssassinate game.localGameState.assassinationTarget)
            )

        RestartGame ->
            ( game
            , restartGame ()
            )


togglePlayerCheck : Player -> Player
togglePlayerCheck p = { p | checked = not p.checked }


subscriptions : Model -> Sub Msg
subscriptions _ = Sub.batch
    [ messageReceiver
          (\str ->
               case D.decodeString serverMessageDecoder str of
                   Ok (GotServerGameMessage msg) ->
                       GotGameMsg (GameRecv msg)
               
                   Ok (GotServerTopMessage msg) ->
                       Recv msg

                   Err e ->
                       JsonError e)
    , storedName StoredName
    ]
