module View exposing (..)

import Array exposing (Array)
import Html.Styled as Html exposing (..)
import Html.Styled.Attributes exposing (..)
import Html.Styled.Events exposing (..)
import Json.Decode as D
import Style
import Css
import Css.Global

import Model exposing (..)

view : Model -> Html Msg
view model =
    div [] <| [Css.Global.global Style.globalStyle] ++ [view_ model]

view_ : Model -> Html Msg
view_ model =
    case model.state of
        Loading ->
            text "Loading..."

        NotStarted startform ->
            Html.map GotStartGameMsg (viewStartForm startform)

        Join name ->
            Html.map GotJoinMsg <| div []
                [ h1 [] [ text "Avalon" ]
                , p [] [ text "A game is currently in progress. Enter your name to join." ]
                , label []
                    [ text "My name: "
                    , input
                          [ type_ "text"
                          , value name
                          , onInput EditName
                          , on "keydown" (ifIsEnter SubmitJoin)
                          ]
                          []
                    ]
                , button [ onClick SubmitJoin ] [ text "Join" ]
                ]

        InGame gamedata ->
            case gamedata.phase of
                GameOver side reason ->
                    div []
                        [ h1 [] [ text "Avalon" ]
                        , text "Game over! The "
                        , case side of
                              Good ->
                                  goodText "good"

                              Evil ->
                                  badText "evil"
                        , text " side won."
                        , br [] []
                        , text reason
                        , br [] []
                        , button [ onClick (GotGameMsg RestartGame) ] [ text "Restart game" ]
                        ]
                _ ->
                    Html.map GotGameMsg (viewGame gamedata)


viewStartForm : StartGameForm -> Html StartGameMsg
viewStartForm startform =
    let
        elements =
            [ h1 [] [ text "Avalon" ]
            , p [] [ text "No game is currently in progress" ]
            , label []
                [ text "Number of players: "
                , select [ onInput SetNumPlayers ]
                    [ option [ value "5" ] [ text "5 (3 good, 2 evil)" ]
                    , option [ value "6" ] [ text "6 (4 good, 2 evil)" ]
                    , option [ value "7" ] [ text "7 (4 good, 3 evil)" ]
                    , option [ value "8" ] [ text "8 (5 good, 3 evil)" ]
                    , option [ value "9" ] [ text "9 (6 good, 3 evil)" ]
                    , option [ value "10" ] [ text "10 (6 good, 4 evil)" ]
                    ]
                ]
            , h2 [] [ text "Optional characters" ]
            ] ++
            characterCheckboxes startform.characters ++
            [ button [ onClick SubmitStartGame ] [ text "Start game" ]
            ]
    in
        div [] elements


viewGame : GameData -> Html GameMsg
viewGame gamedata =
    div []
        [ h1 [] [ text "Avalon" ]
        , unimportantText "You are "
        , roleDescription gamedata.myRole
        , if gamedata.leader /= gamedata.myId then
              viewPlayerList gamedata
          else
              viewTeamPicker gamedata
        , br [] []
        , div [] <|
            case gamedata.phase of
                Vote remaining team ->
                    [ text <| if gamedata.leader == gamedata.myId then
                                  "You have"
                              else
                                  playerName gamedata.players gamedata.leader ++ " has"
                    , text " chosen the team: "
                    , List.map (playerName gamedata.players) team
                        |> String.join ", "
                        |> text
                    , br [] []
                    , button
                          [ onClick (SendVote True)
                          , disabled gamedata.localGameState.alreadyVoted
                          ]
                          [ text "Approve" ]
                    , button
                          [ onClick (SendVote False)
                          , disabled gamedata.localGameState.alreadyVoted
                          ]
                          [ text "Reject" ]
                    , br [] []
                    , let
                          amountString =
                              if remaining == 1 then
                                  "one last chance"
                              else
                                  String.fromInt remaining ++ " chances"
                     in
                         text <|
                             "You have " ++
                             amountString ++
                             " to accept a team before evil wins."
                    ]

                Assassination ->
                    if gamedata.myRole == Assassin then
                        [ text "Choose a person to assassinate"
                        , select [ onInput SetAssassinationTarget ]
                              (Array.toList gamedata.players
                                  |> List.indexedMap
                                      (\i p ->
                                           option
                                               [ value (String.fromInt i) ]
                                               [ text
                                                     (case p of
                                                         Just player ->
                                                             player.name

                                                         Nothing ->
                                                             "")]))
                        , button [ onClick Assassinate ] [ text "Assassinate" ]
                        ]
                    else
                        [ text "The assassin is currently looking for a target..."
                        ]

                _ ->
                    []
        , h2 [] [ text "Quests" ]
        , div [] <|
            case gamedata.phase of
                Quest team ->
                    if List.member gamedata.myId team then
                        [ text "Pick your result for the quest"
                        , br [] []
                        , button
                              [ onClick (SendVote True)
                              , disabled gamedata.localGameState.alreadyVoted
                              ]
                          [ text "Success" ]
                        , button
                              [ onClick (SendVote False)
                              , disabled gamedata.localGameState.alreadyVoted
                              ]
                          [ text "Fail" ]
                        ]
                    else
                        [ text "Currently on a quest: "
                        , List.map (playerName gamedata.players) team
                              |> String.join ", "
                              |> text
                        ]

                _ ->
                    []
        , ol []
            (List.map2
                 (viewQuestListItem gamedata.players)
                 (questsPlayerAmounts (Array.length gamedata.players))
                 (Array.toList gamedata.questResults))
        , h2 [] [ text "Previous vote results"]
        , ol []
            (List.map (viewVoteResult gamedata.players) gamedata.teamVotes)
        , button [ onClick RestartGame ] [ text "Restart game" ]
        ]


viewVoteResult : Array (Maybe Player) -> VoteResult -> Html GameMsg
viewVoteResult players voteResult =
    div []
        [ unimportantText "The team "
        , text <| String.join ", " (playerNamesFromIndices players voteResult.team)
        , unimportantText " was "
        , if voteResult.approved then
              goodText "approved"
          else
              badText "rejected"
        , text "."
        , div [css [Style.voteResultLine]]
            [ unimportantText " Voted approve: "
            , text <| String.join ", "
                 (playerNamesFromIndices players
                 (matchingIndices voteResult.votes True))
            ]
        , div [css [Style.voteResultLine]]
            [ unimportantText "Voted reject: "
            , text <| String.join ", "
                (playerNamesFromIndices players
                     (matchingIndices voteResult.votes False))
            , unimportantText "."
            ]
        ]


playerNamesFromIndices : Array (Maybe Player) -> List Int -> List String
playerNamesFromIndices players indices =
    List.map (\i -> playerName players i) indices


matchingIndices : List a -> a -> List Int
matchingIndices list x =
    List.indexedMap Tuple.pair list
        |> List.filter (\(_, y) -> y == x)
        |> List.map (\(i, _) -> i)


roleDescription : PlayerRole -> Html GameMsg
roleDescription role =
    let
        alignment =
            case role of
                BasicServant -> goodText "Good"
                Merlin -> goodText "Good"
                Percival -> goodText "Good"
                BasicMinion -> badText "Evil"
                Assassin -> badText "Evil"
                Mordred -> badText "Evil"
                Oberon -> badText "Evil"
                Morgana -> badText "Evil"

        description =
            case role of
                BasicServant -> "a loyal servant of Arthur"
                Merlin -> "Merlin"
                Percival -> "Percival"
                BasicMinion -> "a minion of Mordred"
                Assassin -> "the assassin"
                Mordred -> "Mordred"
                Oberon -> "Oberon"
                Morgana -> "Morgana"

    in
        span [] [text description, unimportantText " (",  alignment, unimportantText ")"]


questsPlayerAmounts : Int -> List Int
questsPlayerAmounts numberOfPlayers =
    case numberOfPlayers of
        5 ->
            [2, 3, 2, 3, 3]

        6 ->
            [2, 3, 4, 3, 4]

        7 ->
            [2, 3, 3, 4, 4]

        8 ->
            [3, 4, 4, 5, 5]

        9 ->
            [3, 4, 4, 5, 5]

        10 ->
            [3, 4, 4, 5, 5]

        _ ->
            []


viewPlayerList : GameData -> Html GameMsg
viewPlayerList gamedata =
    div []
        [ h2 [] [ text "Players" ]
        , ol []
            (Array.map viewPlayer gamedata.players
            |> Array.toList)
        , p []
            [ unimportantText "The current leader is: "
            , importantText (playerName gamedata.players gamedata.leader)
            ]
        ]


playerName : Array (Maybe Player) -> Int -> String
playerName players id =
    case Array.get id players of
        Just (Just leader) ->
            leader.name

        _ ->
            ""

        
viewTeamPicker : GameData -> Html GameMsg
viewTeamPicker gamedata =
    let
        picking =
            isPickingTeam gamedata.phase
    in
        div []
            ([ h2 [] [ text "You are currently the leader" ]
             , text "Choose a team for your quest."
             ] ++
             (Array.indexedMap (viewPlayerPicker picking) gamedata.players
             |> Array.toList) ++
             [ button
                   [ onClick StartVote, disabled (not picking) ]
                   [ text "Start voting" ]
             ])


viewPlayerPicker : Bool -> Int -> Maybe Player -> Html GameMsg
viewPlayerPicker picking id maybePlayer =
    div []
        [ label [ ]
              [ input
                    [ type_ "checkbox"
                    , disabled (not picking)
                    , checked (Maybe.map .checked maybePlayer
                                  |> Maybe.withDefault False)
                    , onClick (TogglePlayerCheck id)
                    ]
                    []
              , formatPlayer maybePlayer
              ]
        ]


viewPlayer : Maybe Player -> Html GameMsg
viewPlayer maybePlayer =
    li [] [ formatPlayer maybePlayer ]


formatPlayer : Maybe Player -> Html GameMsg
formatPlayer maybePlayer =
    case maybePlayer of
        Nothing ->
            text ""

        Just player ->
            if player.merlin then
                span [] [text (player.name ++ " ("), neutralText "Merlin", text ")"]
            else if player.evil then
                span [] [text (player.name ++ " ("), badText "Evil", text ")"]
            else
                text player.name


viewQuestListItem : Array (Maybe Player) -> Int -> Maybe QuestResult -> Html GameMsg
viewQuestListItem players playerAmount maybeQuest =
    li []
        [ text <| "(" ++ String.fromInt playerAmount ++ " players): "
        , text
              (case maybeQuest of
                   Just quest ->
                       formatQuest players quest

                   Nothing ->
                       "")
        ]


formatQuest : Array (Maybe Player) -> QuestResult -> String
formatQuest players quest =
    String.concat
        [ if quest.succeeded then
              "Success"
          else
              "Fail"
        , " ("
        , String.fromInt quest.successNumber
        , " success, "
        , String.fromInt quest.failNumber
        , " fail)"
        , ". The team was: "
        , String.join ", " (List.map (\i -> playerName players i) <| Array.toList quest.team)
        , "."
        ]


characterCheckboxes : List Character -> List (Html StartGameMsg)
characterCheckboxes characters =
    List.concatMap
            (\character ->
                 [ label []
                       [ input
                             [ type_ "checkbox"
                             , checked character.checked
                             , onClick (ToggleCheck character.fieldname)
                             ]
                             []
                       , text character.description
                       ]
                 , br [] []
                 ]
            )
            characters


ifIsEnter : msg -> D.Decoder msg
ifIsEnter msg =
  D.field "key" D.string
    |> D.andThen
       (\key ->
            if key == "Enter" then
                D.succeed msg
            else
                D.fail "some other key")


-- Pre-styled elements

styledText : List Css.Style -> String -> Html msg
styledText style value =
    span [css style] [text value]

unimportantText : String -> Html msg
unimportantText =
    styledText [Style.unimportant]

importantText : String -> Html msg
importantText =
    styledText [Style.important]

goodText : String -> Html msg
goodText = styledText [Style.colorGood, Style.important]

badText : String -> Html msg
badText = styledText [Style.colorRed, Style.important]

neutralText : String -> Html msg
neutralText = styledText [Style.colorNeutral, Style.important]
