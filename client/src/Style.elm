module Style exposing (..)

import Css exposing (..)
import Css.Global

globalStyle : List Css.Global.Snippet
globalStyle =
    [ Css.Global.body
        [ backgroundColor (hex "000")
        , color (hex "dedede")
        , fontFamilies ["Source Sans Pro", "sans"]
        ]
    ]

unimportant : Style
unimportant =
    Css.batch [ color (hex "999999") ]


important : Style
important =
    fontWeight bold


colorGood : Style
colorGood =
    color (hex "6daa2c")


colorRed : Style
colorRed =
    color (hex "d04648")


colorNeutral : Style
colorNeutral =
    color (hex "dad45e")


voteResultLine : Style
voteResultLine =
    marginLeft (em 2)
